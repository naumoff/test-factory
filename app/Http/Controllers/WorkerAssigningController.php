<?php

namespace App\Http\Controllers;

use App\Http\Requests\WorkerAssigningRequest;
use App\Http\Resources\WorkerResource;
use App\Models\Worker;
use App\Services\WorkerAssignService;

class WorkerAssigningController extends Controller
{
    public function __construct(
        private readonly WorkerAssignService $workerAssignService,
    ) {}

    public function __invoke(WorkerAssigningRequest $request, Worker $worker): WorkerResource
    {
        $this->workerAssignService->handle($worker, $request->machines);

        return new WorkerResource($worker);
    }
}
