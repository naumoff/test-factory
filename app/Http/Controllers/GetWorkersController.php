<?php

namespace App\Http\Controllers;

use App\Http\Resources\WorkerResource;
use App\Models\Worker;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class GetWorkersController extends Controller
{
    public function __invoke(): AnonymousResourceCollection
    {
        return WorkerResource::collection(Worker::all());
    }
}
