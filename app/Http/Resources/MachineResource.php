<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MachineResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'machine_id' => $this->id,
            'inventory_number' => $this->inventory_number,
            'status' => $this->status,
            'worker' => $this->when(isset($this->worker[0]), function () {
                return $this->worker[0];
            })
        ];
    }
}
