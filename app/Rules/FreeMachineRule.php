<?php

namespace App\Rules;

use App\Enums\MachineAvailability;
use App\Models\Machine;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class FreeMachineRule implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $machine = Machine::find($value);
        if ($machine->status !== MachineAvailability::FREE) {
            $fail(
                sprintf(
                    'The :attribute resource (Id: %d, Inventory Number №%s) must be free before assigning.',
                    $machine->id,
                    $machine->inventory_number
                )
            );
        }
    }
}
