<?php

namespace App\Rules;

use App\Enums\MachineAvailability;
use App\Models\Machine;
use App\Models\Worker;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Contracts\Validation\DataAwareRule;

class BusyMachineRule implements ValidationRule, DataAwareRule
{
    private Worker $worker;

    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $machine = Machine::find($value);
        if ($machine->status !== MachineAvailability::BUSY) {
            $fail(
                sprintf(
                    'The :attribute resource (Id: %d, Inventory Number: №%s) must be busy before releasing.',
                    $machine->id,
                    $machine->inventory_number
                )
            );
        } elseif ($this->worker->machines->where('id', $machine->id)->first() === null) {
            $fail(
                sprintf(
                    'The :attribute resource (Id: %d, Inventory Number №%s) is busy but not used by %s',
                    $machine->id,
                    $machine->inventory_number,
                    $this->worker->full_name,
                )
            );
        }
    }

    public function setData(array $data): static
    {
        $this->worker = $data['worker'];

        return $this;
    }
}
