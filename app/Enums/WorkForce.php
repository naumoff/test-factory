<?php

namespace App\Enums;

use App\Enums\Helpers\BackedEnumToArray;

enum WorkForce: string
{
    use BackedEnumToArray;

    case WORKER_1 = 'Andrey Vorobey';
    case WORKER_2 = 'Sergey Bradobrey';
    case WORKER_3 = 'Mikhail Ohmuril';
    case WORKER_4 = 'Stas Atas';
    case WORKER_5 = 'Artem Shampinion';
    case WORKER_6 = 'Tatiana Populyarna';
    case WORKER_7 = 'Evgeniy Smirenniy';
    case WORKER_8 = 'Ekaterina Stabilna';
    case WORKER_9 = 'Boris Surpriz';

    public function firstName(): string
    {
        return explode(' ', $this->value)[0];
    }

    public function lastName(): string
    {
        return explode(' ', $this->value)[1];
    }
}
