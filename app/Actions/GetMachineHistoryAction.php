<?php

namespace App\Actions;

use App\Models\Machine;
use App\Models\ResourceHistoryRecord;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

readonly class GetMachineHistoryAction
{
    public function execute(Machine $machine, int $perPage = null): LengthAwarePaginator|Collection
    {
        $select = ResourceHistoryRecord::where('machine_id', $machine->id)
            ->join('workers', 'workers.id', '=', 'resource_history_records.worker_id')
            ->select('workers.id as worker_id', 'first_name', 'last_name', 'started_at', 'stopped_at');

        return ($perPage)? $select->paginate($perPage) : $select->get();
    }
}
