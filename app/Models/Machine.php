<?php

namespace App\Models;

use App\Enums\MachineAvailability;
use App\Models\Collections\MachineCollection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Machine extends Model
{
    use HasFactory, SoftDeletes;

    protected $casts = [
        'status' => MachineAvailability::class,
    ];

    public function worker(): BelongsToMany
    {
        return $this->belongsToMany(Worker::class, 'resource_usages', 'machine_id', 'worker_id')
            ->using(ResourceUsage::class)
            ->withTimestamps();
    }

    public function newCollection(array $models = []): MachineCollection
    {
        return new MachineCollection($models);
    }
}
