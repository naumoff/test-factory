<?php

namespace App\Http\Controllers;

use App\Http\Requests\WorkerReleasingRequest;
use App\Http\Resources\WorkerResource;
use App\Models\Worker;
use App\Services\WorkerReleaseService;

class WorkerReleasingController extends Controller
{
    public function __construct(
        private readonly WorkerReleaseService $workerReleaseService,
    ) {}

    public function __invoke(WorkerReleasingRequest $request, Worker $worker): WorkerResource
    {
        $this->workerReleaseService->handle($worker, $request->machines);

        return new WorkerResource($worker);
    }
}
