<?php

namespace App\Rules;

use App\Enums\WorkerAvailability;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class BusyWorkerRule implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if ($value->status !== WorkerAvailability::BUSY) {
            $fail(sprintf('The :attribute resource (%s) must be busy before releasing.', $value->full_name));
        }
    }
}
