<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ResourceUsage extends Pivot
{
    use HasFactory;

    public $incrementing = true;

    protected $table = 'resource_usages';
}
