<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('resource_usages', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('worker_id');
            $table->unsignedBigInteger('machine_id');
            $table->timestamps();
            $table->foreign('worker_id')->references('id')->on('workers')
                ->onDelete('CASCADE');
            $table->foreign('machine_id')->references('id')->on('machines')
                ->onDelete('CASCADE');
            $table->unique(['worker_id', 'machine_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('resource_usages');
    }
};
