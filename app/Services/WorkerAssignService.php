<?php

namespace App\Services;

use App\Actions\UpdateMachinesStatusAction;
use App\Actions\UpdateWorkerStatusAction;
use App\Actions\StartWorkerHistoryAction;
use App\Actions\SetWorkerMachinesAction;
use App\Enums\MachineAvailability;
use App\Enums\WorkerAvailability;
use App\Models\Collections\MachineCollection;
use App\Models\Worker;
use \Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

readonly class WorkerAssignService
{
    public function __construct(
        private SetWorkerMachinesAction $setWorkerMachinesAction,
        private UpdateMachinesStatusAction $updateMachinesStatusAction,
        private UpdateWorkerStatusAction $updateWorkerStatusAction,
        private StartWorkerHistoryAction $startWorkerHistoryAction,
    ) {}

    public function handle(Worker $worker, MachineCollection $machines): void
    {
        try {
            DB::beginTransaction();
            $this->updateWorkerStatusAction->execute($worker, WorkerAvailability::BUSY);
            $this->updateMachinesStatusAction->execute($machines, MachineAvailability::BUSY);
            $this->setWorkerMachinesAction->execute($worker, $machines);
            $this->startWorkerHistoryAction->execute($worker, $machines);
            DB::commit();
        } catch (Exception $exception) {
            DB::rollback(); // something went wrong
            Log::error($exception->getMessage());
            abort(500, 'Worker was not assigned!');
        }
    }
}
