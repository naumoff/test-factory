<?php

namespace App\Services;

use App\Actions\GetWorkerHistoryAction;
use App\Models\Worker;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

readonly class GetWorkerHistoryService
{
    public function __construct(
        private GetWorkerHistoryAction $getWorkerHistoryAction,
    ) {}

    public function handle(Worker $worker, int $perPage = null): LengthAwarePaginator|Collection
    {
        return $this->getWorkerHistoryAction->execute($worker, $perPage);
    }
}
