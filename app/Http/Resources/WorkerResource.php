<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class WorkerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'worker_id' => $this->id,
            'worker_name' => $this->full_name,
            'status' => $this->status,
            'machines' => MachineResource::collection($this->whenLoaded('machines')),
        ];
    }
}
