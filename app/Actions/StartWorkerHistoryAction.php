<?php

namespace App\Actions;

use App\Models\Collections\MachineCollection;
use App\Models\Worker;
use Illuminate\Support\Facades\DB;

readonly class StartWorkerHistoryAction
{
    public function execute(Worker $worker, MachineCollection $machines): void
    {
        if ($machines->count() < 1) {
            return;
        }

        $newRecords = [];
        foreach ($machines as $machine) {
            $newRecords[] = [
                'worker_id' => $worker->id,
                'machine_id' => $machine->id,
                'started_at' => now(),
                'stopped_at' => null,
            ];
        }

        DB::table('resource_history_records')->insert($newRecords);
    }
}
