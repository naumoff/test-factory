<?php

namespace App\Http\Controllers;

use App\Http\Resources\WorkerResource;
use App\Models\Worker;

class GetWorkerAssignmentController extends Controller
{
    public function __invoke(Worker $worker): WorkerResource
    {
        $worker->load('machines');

        return new WorkerResource($worker);
    }
}
