<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('/login', \App\Http\Controllers\LoginController::class);

Route::middleware(['auth:sanctum', 'abilities:factory.management'])->group(function() {
    Route::get('workers', \App\Http\Controllers\GetWorkersController::class);
    Route::get('machines', \App\Http\Controllers\GetMachinesController::class);
    Route::post('workers/{worker}/assign', \App\Http\Controllers\WorkerAssigningController::class);
    Route::post('workers/{worker}/release', \App\Http\Controllers\WorkerReleasingController::class);
    Route::get('workers/{worker}', \App\Http\Controllers\GetWorkerAssignmentController::class);
    Route::get('machines/{machine}', \App\Http\Controllers\GetMachineAssignmentController::class);
    Route::get('workers/{worker}/history', \App\Http\Controllers\GetWorkerHistoryController::class);
    Route::get('machines/{machine}/history', \App\Http\Controllers\GetMachineHistoryController::class);
});
