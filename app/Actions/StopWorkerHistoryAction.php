<?php

namespace App\Actions;

use App\Models\Collections\MachineCollection;
use App\Models\ResourceHistoryRecord;
use App\Models\Worker;

readonly class StopWorkerHistoryAction
{
    public function execute(Worker $worker, MachineCollection $machines): void
    {
        if ($machines->count() < 1) {
            return;
        }

        ResourceHistoryRecord::whereNull('stopped_at')
            ->where('worker_id', $worker->id)
            ->whereIn('machine_id', $machines->pluck('id')->toArray())
            ->update(['stopped_at' => now()]);
    }
}
