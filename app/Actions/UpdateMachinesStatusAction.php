<?php

namespace App\Actions;

use App\Enums\MachineAvailability;
use App\Models\Collections\MachineCollection;
use App\Models\Machine;

readonly class UpdateMachinesStatusAction
{
    public function execute(MachineCollection $machines, MachineAvailability $status): void
    {
        Machine::whereIn('id', $machines->pluck('id')->toArray())->update(['status' => $status->value]);
    }
}
