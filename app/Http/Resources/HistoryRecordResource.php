<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class HistoryRecordResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            # worker history:
            'worker_id' => $this->whenHas('worker_id'),
            'worker_name' => $this->when($this->first_name, function() {
                return "$this->first_name $this->last_name";
            }),

            # machine history:
            'machine_id' => $this->whenHas('machine_id'),
            'inventory_number' => $this->whenHas('inventory_number'),

            # history period:
            'started_at' => $this->started_at,
            'stopped_at' => $this->stopped_at ?? 'till now',
        ];
    }
}
