<?php

namespace Database\Seeders;

use App\Enums\WorkerAvailability;
use App\Enums\WorkForce;
use App\Models\Worker;
use Illuminate\Database\Seeder;

class WorkerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        foreach (WorkForce::cases() as $worker) {
            Worker::firstOrCreate(
                ['first_name' => $worker->firstName(), 'last_name' => $worker->lastName()],
                [
                    'status' => WorkerAvailability::FREE->value,
                ]
            );
        }
    }
}
