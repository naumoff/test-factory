<?php

namespace App\Models;

use App\Enums\WorkerAvailability;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Worker extends Model
{
    use HasFactory, SoftDeletes;

    protected $casts = [
        'status' => WorkerAvailability::class,
    ];

    public function machines(): BelongsToMany
    {
        return $this->belongsToMany(Machine::class, 'resource_usages', 'worker_id', 'machine_id')
            ->using(ResourceUsage::class)
            ->withTimestamps();
    }

    public function getFullNameAttribute(): string
    {
        return "{$this->first_name} {$this->last_name}";
    }
}
