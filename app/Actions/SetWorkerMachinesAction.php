<?php

namespace App\Actions;

use App\Models\Collections\MachineCollection;
use App\Models\Worker;

readonly class SetWorkerMachinesAction
{
    public function execute(Worker $worker, MachineCollection $machines): void
    {
        $worker->machines()->attach($machines->pluck('id')->toArray());
        $worker->load('machines');
    }
}
