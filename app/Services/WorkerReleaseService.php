<?php

namespace App\Services;

use App\Actions\DropWorkerMachinesAction;
use App\Actions\UpdateMachinesStatusAction;
use App\Actions\UpdateWorkerStatusAction;
use App\Actions\StopWorkerHistoryAction;
use App\Enums\MachineAvailability;
use App\Enums\WorkerAvailability;
use App\Models\Collections\MachineCollection;
use App\Models\Worker;
use Illuminate\Support\Facades\DB;
use \Exception;
use Illuminate\Support\Facades\Log;

readonly class WorkerReleaseService
{
    public function __construct(
        private UpdateWorkerStatusAction $updateWorkerStatusAction,
        private UpdateMachinesStatusAction $updateMachinesStatusAction,
        private DropWorkerMachinesAction $dropWorkerMachinesAction,
        private StopWorkerHistoryAction $stopWorkerHistoryAction,
    ) {}

    public function handle(Worker $worker, MachineCollection $machines): void
    {
        try {
            DB::beginTransaction();
            if ($machines->count() === $worker->machines()->count()) {
                $this->updateWorkerStatusAction->execute($worker, WorkerAvailability::FREE);
            }
            $this->updateMachinesStatusAction->execute($machines, MachineAvailability::FREE);
            $this->dropWorkerMachinesAction->execute($worker, $machines);
            $this->stopWorkerHistoryAction->execute($worker, $machines);
            DB::commit();
        } catch (Exception $exception) {
            DB::rollback(); // something went wrong
            Log::error($exception->getMessage());
            abort(500, 'Worker was not released!');
        }
    }
}
