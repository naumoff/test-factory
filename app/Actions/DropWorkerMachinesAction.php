<?php

namespace App\Actions;

use App\Models\Collections\MachineCollection;
use App\Models\Worker;

readonly class DropWorkerMachinesAction
{
    public function execute(Worker $worker, MachineCollection $machines): void
    {
        $worker->machines()->detach($machines->pluck('id')->toArray());
        $worker->load('machines');
    }
}
