<?php
namespace App\Http\Controllers;

use App\Http\Resources\MachineResource;
use App\Models\Machine;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class GetMachinesController extends Controller
{
    public function __invoke(): AnonymousResourceCollection
    {
        return MachineResource::collection(Machine::all());
    }
}
