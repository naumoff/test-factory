<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::firstOrCreate(
            ['id' => config('users.api_user_id')],
            [
                'name' => config('users.user_name'),
                'email' => config('users.user_email'),
                'email_verified_at' => now(),
                'password' => Hash::make(config('users.user_password')),
            ]
        );
    }
}
