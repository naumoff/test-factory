<?php

namespace App\Http\Controllers;

use App\Http\Resources\MachineResource;
use App\Models\Machine;

class GetMachineAssignmentController extends Controller
{
    public function __invoke(Machine $machine): MachineResource
    {
        $machine->load('worker');

        return new MachineResource($machine);
    }
}
