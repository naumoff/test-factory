<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>


## Installation requirements:
- PHP 8.2
- MySQL 8.0

## Installation setup:
### step 1
- Set env credentials in env file for Mysql DB and API USER:
```
# * * * * * CUSTOM  * * * * *
USER_NAME='api user'
USER_EMAIL='andrey.naumoff@gmail.com'
USER_PASSWORD='12345'
```
### step 2
- Run console commands:
```
php artisan:migrate
php artisan db:seed
```
### step3
- get access token with user credentials via: POST /api/login

![screenshot](/storage/screenshots/screen1.png?raw=true)

### step 4
- copy received token to rest of requests:

![screenshot](/storage/screenshots/screen2.png?raw=true)

### step 5
- follow api endpoints in api.php route file with received token:

![screenshot](/storage/screenshots/screen3.png?raw=true)

## Task
Create an application that will store and provide information about which machine a worker is currently operating or has worked on previously.

Initial data:
1. There are workers:
a. Andrey
b. Sergey
c. Mikhail
d. Stas
e. Artem
f. Tatiana
g. Eugene
h. Katya
i. Boris

2. There are machines:
a. 44
b. 56
c. 23
d. 78
e. 102

You need to implement a REST API that provides the following capabilities:
1. Get a list of workers.
2. Get a list of machines.
3. Assign a worker to a machine. In other words, temporarily associate a worker with a machine. Implement a constraint that only one worker can work on a machine at a time, but one worker can work on multiple machines simultaneously.
4. Mark that a worker has finished working on a machine. In other words, remove the temporary association between the worker and the machine.
5. Get current information about a worker. This includes which machines they are currently operating.
6. Get current information about a machine. This includes which worker is currently operating it.
7. Get a history of a worker's activities. This should include when the worker started and finished work on a machine. Implement pagination for the history.
8. Get a history of a machine's activities. This should include when workers started and finished work on the machine and which worker was involved.

Technology Requirements:
1. PHP framework Laravel or Lumen.
2. MySQL database.
