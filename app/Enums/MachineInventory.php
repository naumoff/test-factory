<?php

namespace App\Enums;

use App\Enums\Helpers\BackedEnumToArray;

enum MachineInventory: int
{
    use BackedEnumToArray;

    case INVENTORY_N_44 = 44;
    case INVENTORY_N_56 = 56;
    case INVENTORY_N_23 = 23;
    case INVENTORY_N_78 = 78;
    case INVENTORY_N_102 = 102;
}
