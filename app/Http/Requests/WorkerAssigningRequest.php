<?php

namespace App\Http\Requests;

use App\Models\Collections\MachineCollection;
use App\Models\Machine;
use App\Rules\FreeMachineRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;

class WorkerAssigningRequest extends FormRequest
{
    public MachineCollection $machines;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'machines' => ['array', 'required', 'min:1'],
            'machines.*' => ['integer', 'exists:machines,id', new FreeMachineRule()],
        ];
    }

    public function withValidator(Validator $validator): void
    {
        if(! $validator->fails()){
            $this->machines = Machine::whereIn('id', $this->get('machines'))->get();
        }
    }
}
