<?php

namespace App\Actions;

use App\Enums\WorkerAvailability;
use App\Models\Worker;

readonly class UpdateWorkerStatusAction
{
    public function execute(Worker $worker, WorkerAvailability $status): void
    {
        $worker->status = $status;
        $worker->save();
    }
}
