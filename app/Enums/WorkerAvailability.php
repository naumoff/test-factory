<?php

namespace App\Enums;

use App\Enums\Helpers\BackedEnumToArray;

enum WorkerAvailability: string
{
    use BackedEnumToArray;

    case FREE = 'free';
    case BUSY = 'busy';
}
