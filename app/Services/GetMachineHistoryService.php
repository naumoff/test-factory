<?php

namespace App\Services;

use App\Actions\GetMachineHistoryAction;
use App\Models\Machine;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

readonly class GetMachineHistoryService
{
    public function __construct(
        private GetMachineHistoryAction $getMachineHistoryAction,
    ) {}

    public function handle(Machine $machine, int $perPage = null): LengthAwarePaginator|Collection
    {
        return $this->getMachineHistoryAction->execute($machine, $perPage);
    }
}
