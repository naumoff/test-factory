<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ResourceHistoryRecord extends Model
{
    use HasFactory;

    protected $table = 'resource_history_records';

    public $timestamps = false;
}
