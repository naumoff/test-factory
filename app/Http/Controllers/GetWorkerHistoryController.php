<?php

namespace App\Http\Controllers;

use App\Http\Resources\HistoryRecordResource;
use App\Models\Worker;
use App\Services\GetWorkerHistoryService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class GetWorkerHistoryController extends Controller
{
    public function __construct(
        private readonly GetWorkerHistoryService $getWorkerHistoryService,
    ) {}

    public function __invoke(Worker $worker): AnonymousResourceCollection
    {
        $workerHistory = $this->getWorkerHistoryService->handle($worker, 15);

        return HistoryRecordResource::collection($workerHistory);
    }
}
