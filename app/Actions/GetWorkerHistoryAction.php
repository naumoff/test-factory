<?php

namespace App\Actions;

use App\Models\ResourceHistoryRecord;
use App\Models\Worker;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

readonly class GetWorkerHistoryAction
{
    public function execute(Worker $worker, int $perPage = null): LengthAwarePaginator|Collection
    {
        $select = ResourceHistoryRecord::where('worker_id', $worker->id)
            ->join('machines', 'machines.id', '=', 'resource_history_records.machine_id')
            ->select('machines.id as machine_id', 'inventory_number', 'started_at', 'stopped_at');

        return ($perPage)? $select->paginate($perPage) : $select->get();
    }
}
