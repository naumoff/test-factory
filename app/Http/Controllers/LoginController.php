<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function __invoke(LoginRequest $request): JsonResponse
    {
        if (Auth::attempt($request->loginDTO->toArray())) {
            /** @var User $user */
            $user = Auth::user();
            $user->tokens()->delete();
            $success['token'] = $user->createToken('token-name', ['factory.management'])->plainTextToken;

            return response()->json($success);
        }
        abort(401, 'wrong login credentials');
    }
}
