<?php

namespace Database\Seeders;

use App\Enums\MachineAvailability;
use App\Enums\MachineInventory;
use App\Models\Machine;
use Illuminate\Database\Seeder;

class MachineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        foreach (MachineInventory::array() as $inventoryNumber) {
            Machine::firstOrCreate(
                ['inventory_number' => $inventoryNumber],
                [
                    'status' => MachineAvailability::FREE->value,
                ]
            );
        }
    }
}
