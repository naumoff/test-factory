<?php

namespace App\Http\Controllers;

use App\Http\Resources\HistoryRecordResource;
use App\Models\Machine;
use App\Services\GetMachineHistoryService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class GetMachineHistoryController extends Controller
{
    public function __construct(
        private readonly GetMachineHistoryService $getMachineHistoryService,
    ) {}

    public function __invoke(Machine $machine): AnonymousResourceCollection
    {
        $machineHistory = $this->getMachineHistoryService->handle($machine, 15);

        return HistoryRecordResource::collection($machineHistory);
    }
}
